package ar.com.firstdata.dao;

import java.util.List;

import ar.com.firstdata.domain.model.Pedido;


public interface PedidosDAO {
	
	void insertOrUpdate(Pedido pedido);
	
	void delete(Pedido pedido);
	
	Pedido select(Integer idPedido);
	 
	List<Pedido> selectAll();
}
