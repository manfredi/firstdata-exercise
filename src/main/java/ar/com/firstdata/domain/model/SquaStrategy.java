package ar.com.firstdata.domain.model;

public class SquaStrategy implements PaymentStrategy {

	@Override
	public Double calculateRate() {
 
		return 0.1;
	}

	@Override
	public String getType() {

		return "SQUA";
	}

	
}
