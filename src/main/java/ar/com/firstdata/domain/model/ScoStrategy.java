package ar.com.firstdata.domain.model;

public class ScoStrategy implements PaymentStrategy {

	@Override
	public Double calculateRate() {
 
		return 1.0;
	}

	@Override
	public String getType() {

		return "SCO";
	}

	
}
