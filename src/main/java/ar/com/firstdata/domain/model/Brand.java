package ar.com.firstdata.domain.model;

  
public class Brand {
	
	private String nameBrand; 
	private Double rateCalculated; 
	 

	public Brand(PaymentStrategy paymentMethod) {
		super();
	
		this.nameBrand = paymentMethod.getType();
		this.rateCalculated = paymentMethod.calculateRate();
	}


	public String getNameBrand() {
		return nameBrand;
	}

	
	
	public Double getRateCalculated() {
		return rateCalculated;
	}




	 
 
}
