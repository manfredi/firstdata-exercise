package ar.com.firstdata.domain.model;

import java.util.Date;
 
public class Card  {
	
	private Integer numbeCard;
 	private String  cardholder;
	private Brand   brand;
	private Date    expirationDate;
	
	
	public Integer getNumbeCard() {
		return numbeCard;
	}
	public void setNumbeCard(Integer numbeCard) {
		this.numbeCard = numbeCard;
	}
	public String getCardholder() {
		return cardholder;
	}
	public void setCardholder(String cardholder) {
		this.cardholder = cardholder;
	}
	public Brand getBrand() {
		return brand;
	}
	public void setBrand(Brand brand) {
		this.brand = brand;
	}
	public Date getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}
  
	public Boolean valid() {

		Date now = new Date();

		if (expirationDate.after(now)) {
			return false;
		}
		return true;
	}
	
	
	public Boolean executeTransaction(Double amount) {

		Date now = new Date();

		if (expirationDate.after(now)) {
			return false;
		}
		return true;
	}
	
 	 
}
