package ar.com.firstdata.domain.model;

public class PereStrategy implements PaymentStrategy {

	@Override
	public Double calculateRate() {
 
		return 1.9;
	}

	@Override
	public String getType() {

		return "PERE";
	}

	
}
