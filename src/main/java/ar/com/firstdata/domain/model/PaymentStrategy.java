package ar.com.firstdata.domain.model;

public interface PaymentStrategy {
	
	public Double calculateRate();
    public String getType();


}
