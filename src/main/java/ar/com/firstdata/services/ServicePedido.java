package ar.com.firstdata.services;

import java.util.List;

import ar.com.firstdata.domain.model.Pedido;

public interface ServicePedido {
	
	
	public  Pedido generarPedido(Pedido pedido);
	
	public  Pedido editarPedido(Pedido pedido);
		
	public  Pedido borrarPedido(Pedido pedido);
	
	public  Pedido buscarPorId(Integer pedido);
	
	public  List<Pedido> buscar() ;

}
