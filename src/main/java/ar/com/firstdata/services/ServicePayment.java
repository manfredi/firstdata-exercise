package ar.com.firstdata.services;

import java.util.List;

import ar.com.firstdata.domain.model.Card;
 
public interface ServicePayment {
	
	public  Boolean imprimirFactura(Integer idfactura);
	public  Boolean enviarInfoTC(Card card);
	public  Boolean informarPago(Card card);
	public  Boolean actualizarSaldo(Card card);

	 
}
