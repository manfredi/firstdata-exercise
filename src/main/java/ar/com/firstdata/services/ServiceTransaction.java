package ar.com.firstdata.services;

import ar.com.firstdata.domain.model.Card;


public interface ServiceTransaction {
	
 
	public  Boolean validateTransaction(Card card);
 
}
